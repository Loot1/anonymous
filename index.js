require("dotenv").config({path:".env"})
const { Client } = require("discord.js")
const client = new Client()

client.login()

client.on("ready", () => console.log("Anonymous bot is on !"))

function errormsg(message,desc) {
	message.reply(new MessageEmbed()
	.setColor('4e6f7b')
	.setTitle("Erreur")
	.setDescription(desc)
	.setTimestamp()
	)
}

client.on("message", (message) => {
	if(message.author.bot || message.channel.type != 'dm' || message.type != 'DEFAULT') return
	const anonymousChannel = client.channels.cache.get(`${process.env.CHANNEL}`)
	if(!anonymousChannel) return errormsg(message,"Le canal anonyme n'est pas défini, veuillez contacter le propriétaire du serveur.")
	if(message.content) {
		anonymousChannel.send(message.content)
		if(message.attachments.size !== 0) errormsg(message,"Il est impossible d'envoyer les fichiers contenu dans le message mais le texte a bien été envoyé.")
	} else errormsg(message,"Il est impossible d'envoyer des fichiers.")
})